# Buildit Front End Exercise

Please take your time to read this README carefully until the end before starting the task.

## Problem

Shilpa is working as a Talent Recruiter in your organization. She has to sift through a lot of applications every day to find the best possible talent. In order to help her do it efficiently, she uses a small application which displays all the relevant candidate information in a table:
![Screenshot](mockup.png)
As your organization has started to get more applications, the table got too cluttered with information and she needs a better way to filter the incoming applications.

## Goal

You need to build an application with an enhanced user interface. Below are the requirements you have received from Shilpa:

- See the list with all applications showing the following data:
  - Name (string)
  - Email (string)
  - Age (string - to be calculated from birthday)
  - Years of experience (number)
  - Position applied (string)
  - Date of application (string)
  - Status of the application (string: approved | rejected |waiting)
- Sort By
  - Position Applied
  - Years of experience
  - Date of Application
- Filter By
  - Status
  - Position Applied

You can improve the UI in whatever way you see fit. To receive the candidates list, you can use below API:
https://immense-bastion-96460.herokuapp.com/v1/candidates

This endpoint may occasionally return errors. Please process those errors when you develop the solution.

## Non functional requirements

- Use React.js or Angular to accomplish the task. Show of yours skills and knowledge on the ecosystem that you are using.
- Feel free to use any state management and routing libraries(if needed).
- Please do not use any list rendering libraries (e.g. react table)

## Time Constraint

We allow **4 working days** for the task and expect completion within 18 hours. We expect a reply within one week upon receiving the email.

## Returning The Result To Us

1. Make sure all source changes are included.
2. Exclude any file that is listed in .gitignore to reduce the size.
3. Provide instructions on how to bootstrap your app (in Readme.md of your repository).
4. Zip the directory and send it to the email address you received the assignment from.

## We will look at the following:

- Functionality
- Code Quality
- UI and styling structure
- Architecture and Scalability

Good luck!
