# Buildit Front End Exercise

Please take your time to read this README carefully until the end before starting the task.

## Problem

Our organization has several tradional web applications which uses libraries such as JQuery and Vanilla Javascript on the front end side. As the client side complexity increased from time to time, we have decided to migrate our existing applications to Singple page applications. These single page applications use REST APIs to uodate abd query any information. After several discussions, we have agreed to use React and Typescript on the client side. As an initial step we need to develop a re-usable component library to be used across different projects. Components such as progress bars, alert messages, data grid etc can be created and maintained inside this library.

Your task is to setup this re-usablelibrary. Use webpack or rollup as the bundler. You also need to create a skeleton loading indicator component as per the wireframe given below
![Screenshot](preview.png)

## Goal

The goal of this exercise is to create a re-usable library using React and written with Typescript.

The focus is on setting up the library and then to create a skeleton loading indicator component. You should provide option to configure with width as well as number of lines. You also need to find all the possible properties this component can have. Add Proper tests to ensure that component works as per your expectation. You need to use tools like storybook to show case your component.

## Time Constraint

We allow **two work day** for the task and expect completion within 6 to 8 hours. We expect a reply within 48 hours upon receiving the email.

## Returning The Result To Us

1. Make sure all source changes are included.
2. Exclude any file that is listed in .gitignore to reduce the size.
3. Zip the directory and send it to the email address you received the assignment from.

Good luck!
